streamGraph = function() {
    var margin = {top: 10, right: 10, bottom: 10, left: 10};

    var width = 800,
        height = 600;

    var xDomain = null;

    var bisectDate = d3.bisector(function(dd) {
            return dd.date;
        }).left;

    var format = d3.time.format("%b %e, %Y %H:%M");

    function streamGraph(selection) {
        selection.each(function(data) {
            // -- main content axis --
            var x = d3.time.scale()
                    .range([0, width - margin.left - margin.right]);

            var y = d3.scale.linear()
                    .range([height - margin.top - margin.bottom, 0]);

            var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    .tickSize(-height + margin.top + margin.bottom);

            var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickSize(-width + margin.left + margin.right);

            // -- content areas --
            var area1 = d3.svg.area()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y0(function(d) {
                        return y(d.low);
                    })
                    .y1(function(d) {
                        return y(d.tenth);
                    });

            var area2 = d3.svg.area()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y0(function(d) {
                        return y(d.tenth);
                    })
                    .y1(function(d) {
                        return y(d.twentyfifth);
                    });

            var area3 = d3.svg.area()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y0(function(d) {
                        return y(d.twentyfifth);
                    })
                    .y1(function(d) {
                        return y(d.seventyfifth);
                    });

            var area4 = d3.svg.area()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y0(function(d) {
                        return y(d.seventyfifth);
                    })
                    .y1(function(d) {
                        return y(d.ninetyth);
                    });

            var area5 = d3.svg.area()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y0(function(d) {
                        return y(d.ninetyth);
                    })
                    .y1(function(d) {
                        return y(d.high);
                    });

            var median = d3.svg.line()
                    .interpolate("cardinal")
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y(function(d) {
                        return y(d.median);
                    });

            var current = d3.svg.line()
                    .defined(function(dd) {
                        return dd.current !== -500;
                    })
                    .x(function(dd) {
                        return x(dd.date);
                    })
                    .y(function(dd) {
                        return y(dd.current);
                    });

            var topofdyke = d3.svg.line()
                    .defined(function(dd) {
                        return dd.current !== -1;
                    })
                    .x(function(dd) {
                        return x(dd.date);
                    })
                    .y(function(dd) {
                        return y(dd.topofdyke);
                    });

            if(xDomain) {
                x.domain(xDomain);
            } else {
                x.domain(d3.extent(data.data1.map(function(d) {return d.date;})));
            }
            x.nice();
            var maxofarray1 = d3.max(data.data1, function(d) {return d.high;}) || 0;
            var maxofarray2 = d3.max(data.data2, function(d) {return d.current;}) || 0;
            y.domain([0, Math.max(maxofarray1, maxofarray2)]).nice();

            var svg = d3.select(this);
            var main = svg.select("g.main");

            if(!main.empty()) { // just redraw
                var dataFiltered = data.data1.filter(function(d, i) {
                    if ((d.date >= xDomain[0]) && (d.date <= xDomain[1]))
                    {
                        return d.high;
                    }
                });
                var dataFiltered2 = data.data2.filter(function(d, i) {
                    if ((d.date >= xDomain[0]) && (d.date <= xDomain[1]))
                    {
                        return d.current;
                    }
                });
                var maxofarray1 = d3.max(dataFiltered, function(d) {return d.high;}) || 0;
                var maxofarray2 = d3.max(dataFiltered2, function(d) {return d.current;}) || 0;
                y.domain([0, Math.max(maxofarray1, maxofarray2)]).nice();
                
                main.selectAll("path.area1").attr("d", area1);
                main.selectAll("path.area2").attr("d", area2);
                main.selectAll("path.area3").attr("d", area3);
                main.selectAll("path.area4").attr("d", area4);
                main.selectAll("path.area5").attr("d", area5);
                main.selectAll("path.median").attr("d", median);
                main.selectAll("path.current").attr("d", current);
                main.selectAll("path.topofdyke").attr("d", topofdyke);
                main.selectAll("g.x.axis").call(xAxis)
                    .selectAll("text")
                    .attr("y", 10);
                main.selectAll("g.y.axis").call(yAxis);

                // update overlay
                var focus1 = main.select("g.focus1");
                var focus2 = main.select("g.focus2");
                main.select("rect.overlay")
                        .on("mousemove", function() {
                            var x0 = x.invert(d3.mouse(this)[0]),
                                i = bisectDate(dataFiltered2, x0, 1),
                                dd0 = dataFiltered2[i - 1],
                                dd1 = dataFiltered2[i],
                                dd = dd1 === undefined || x0 - dd0.date <= dd1.date - x0 ? dd0 : dd1;
                            if (dd.current !== -1) {
                                focus1.style("display", null);
                                focus2.style("display", null);
                                focus1.attr("transform", "translate(" + x(dd.date) + "," + y(dd.current) + ")");
                                focus2.attr("transform", "translate(" + (width - margin.left - margin.right - 15) + "," + 10 + ")");
                                focus1.select("text").text();
                                focus2.select("text").text(format(dd.date) + " - " + dd.current.toFixed(2) + " m");
                            } else {
                                focus1.style("display", "none");
                                focus2.style("display", "none");
                            }
                        });
                return;
            }
                    
            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                .append("rect")
                    .attr("width", width - margin.left - margin.right)
                    .attr("height", height - margin.top - margin.bottom);

            main = svg.append("g")
                    .attr("class", "main")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            main.append("text")
                .attr("transform", "translate(" + (width - margin.left - margin.right)/2 + ",-5)")
                .style("text-anchor", "middle")
                .text(data.station);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "area1")
                    .attr("d", area1);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "area2")
                    .attr("d", area2);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "area3")
                    .attr("d", area3);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "area4")
                    .attr("d", area4);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "area5")
                    .attr("d", area5);

            main.append("path")
                    .datum(data.data1)
                    .attr("class", "median")
                    .attr("d", median);

            main.append("path")
                    .datum(data.data1)
                    .style("stroke-dasharray", ("5, 5"))
                    .attr("class", "topofdyke")
                    .attr("d", topofdyke);

            main.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + (height - margin.top - margin.bottom) + ")")
                    .call(xAxis)
                    .selectAll("text")
                    .attr("y", 10);

            main.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .attr("x", -6)
                    .style("text-anchor", "end")
                    .text("Stage (m)");

            main.append("path")
                    .datum(data.data2)
                    .attr("class", "current")
                    .attr("d", current);

            var focus1 = main.append("g")
                    .attr("class", "focus1")
                    .style("display", "none");

            focus1.append("circle")
                    .attr("r", 3);

            focus1.append("text")
                    .attr("x", 9)
                    .attr("dy", ".35em");

            var focus2 = main.append("g")
                    .attr("class", "focus2")
                    .style("display", "none")
                    .style("text-anchor", "end");

            focus2.append("path")
                    .attr("x", 2)
                    .attr("y0", height - margin.top - margin.bottom)
                    .attr("y1", height - margin.top - margin.bottom + 5);

            focus2.append("text")
                    .attr("x", 9)
                    .attr("dy", ".35em");

            main.append("rect")
                    .attr("class", "overlay")
                    .attr("width", width - margin.left - margin.right)
                    .attr("height", height - margin.top - margin.bottom)
                    .on("mouseover", function() {
                        focus1.style("display", null);
                        focus2.style("display", null);
                    })
                    .on("mouseout", function() {
                        focus1.style("display", "none");
                        focus2.style("display", "none");
                    })
                    .on("mousemove", mousemove);

            function mousemove() {
                var x0 = x.invert(d3.mouse(this)[0]),
                        i = bisectDate(data.data2, x0, 1),
                        dd0 = data.data2[i - 1],
                        dd1 = data.data2[i],
                        dd = dd1 === undefined || x0 - dd0.date <= dd1.date - x0 ? dd0 : dd1;
                if (dd.current !== -1) {
                    focus1.style("display", null);
                    focus2.style("display", null);
                    focus1.attr("transform", "translate(" + x(dd.date) + "," + y(dd.current) + ")");
                    focus2.attr("transform", "translate(" + (width - margin.left - margin.right - 15) + "," + 10 + ")");
                    focus1.select("text").text();
                    focus2.select("text").text(format(dd.date) + " - " + dd.current.toFixed(2) + " m");
                } else {
                    focus1.style("display", "none");
                    focus2.style("display", "none");
                }
            }
        });
    }

    streamGraph.width = function(value) {
      if (!arguments.length) return width;
      width = value;
      return streamGraph;
    };
  
    streamGraph.height = function(value) {
      if (!arguments.length) return height;
      height = value;
      return streamGraph;
    };

    streamGraph.margin = function(value) {
      if (!arguments.length) return margin;
      margin = value;
      return streamGraph;
    };

    streamGraph.xDomain = function(value) {
      if (!arguments.length) return xDomain;
      xDomain = value;
      return streamGraph;
    };

    return streamGraph;
}
