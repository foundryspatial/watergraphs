$(document).ready(function() {
    var stations = getURLParameter("stns").split(",");

    var q = queue();
    stations.forEach(function(s) {
        q.defer(d3.json, "resources/data/StageDayAverage.php?stn=" + s);
        q.defer(d3.json, "resources/data/StageCurrent.php?stn=" + s);
    });
    q.awaitAll(function(error, results) {
        var data = [];
        var xs = [];
        var parseDate = d3.time.format("%Y%m%d").parse;
        var parseDateTime = d3.time.format("%Y-%m-%d %H:%M:%S%Z").parse;

        results.forEach(function(res, i) {
            if(i % 2) { // StageCurrent
                res.forEach(function(dd) {
                    dd.date = parseDateTime(dd.datetime);
                    if (dd.current !== -1) {
                        dd.current = +dd.current;
                    }
                });
                data[data.length-1].data2 = res;
            } else { // StageDayAverage
                res.forEach(function(d) {
                    d.date = parseDate(d.date);
                    xs.push(d.date);
                    d.low = +d.low;
                    d.high = +d.high;
                    d.tenth = +d.tenth;
                    d.twentyfifth = +d.twentyfifth;
                    d.median = +d.median;
                    d.seventyfifth = +d.seventyfifth;
                    d.ninetyth = +d.ninetyth;
                    d.topofdyke = +d.topofdyke;
                });
                data.push({station: stations[i/2], data1: res});
            }
        });

        var xExtent = d3.extent(xs);

        var width = 970,
            height = 450,
            brushHeight = 50,
            margin = {top: 20, right: 25, bottom: 30, left: 30};

        var chart = streamGraph()
            .width(width)
            .height(height)
            .margin(margin)
            .xDomain(xExtent);

        d3.select("div.contents")
            .selectAll("div.graph")
            .data(data)
            .enter()
            .append("div")
            .attr("class", "graph")
            .append("svg")
            .attr("width", chart.width())
            .attr("height", chart.height())
            .call(chart);

        // brush
        var brushScale = d3.time.scale()
                .domain(xExtent)
                .range([0, width - margin.left - margin.right]).nice(),
            brushAxis = d3.svg.axis().scale(brushScale)
                .tickSize(brushHeight - margin.top, 0, (brushHeight - margin.top)/2),
            brush = d3.svg.brush().x(brushScale)
                .on("brush", brushed);

        var axisG = d3.select("div.footer").insert("svg", "div")
            .attr("width", chart.width())
            .attr("height", brushHeight)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + (brushHeight - margin.top)/2 + ")");
        axisG.attr("class", "x brushAxis")
            .call(brushAxis)
            .selectAll("line")
            .attr("transform", "translate(0," + -(brushHeight - margin.top)/2 + ")");
        axisG.selectAll("text")
            .attr("transform", "translate(0," + -(brushHeight - margin.top)/2 + ")");
        axisG.append("g")
            .attr("class", "x brush")
            .call(brush)
            .selectAll("rect")
            .attr("y", -(brushHeight - margin.top)/2)
            .attr("height", brushHeight - margin.top);

        function brushed() {
            var ex = brush.extent();
            if(ex[1] > xExtent[1]) {
                brush.extent([ex[0], xExtent[1]]);
            }
            if(ex[0] < xExtent[0]) {
                brush.extent([xExtent[0], ex[1]]);
            }
            chart.xDomain(brush.empty() ? xExtent : brush.extent());
            d3.select("div.contents")
                .selectAll("div.graph")
                .call(chart);
        }
        
        d3.select("#reset").on("click", function() {
            brush.clear();
            axisG.select(".brush").call(brush);
            brushed();
        });
        d3.select("#week").on("click", function() {
            brush.extent([xExtent[1] - 86400 * 1000 * 7 * 2, xExtent[1]]);
            axisG.select(".brush").call(brush);
            brushed();
        });
        d3.select("#day").on("click", function() {
            brush.extent([xExtent[1] - 86400 * 1000, xExtent[1]]);
            axisG.select(".brush").call(brush);
            brushed();
        });
    });
});

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}
